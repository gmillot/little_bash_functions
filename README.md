
#### DESCRIPTION

Little bash Functions contains the 9 following functions:
show_time_fun
test_tab_in_file_fun
test_space_in_file_fun
space_replacement_in_file_fun
file_pattern_detection_fun
test_column_nb_perline_in_file_fun
single_path_with_regex_fun
var_existence_check_fun
file_exten_verif_name_recov_fun


#### LICENCE

This package of scripts can be redistributed and/or modified under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details at https://www.gnu.org/licenses or in the Licence.txt attached file.


#### CREDITS

Gael A. Millot, Hub-C3BI, Institut Pasteur, USR 3756 IP CNRS, Paris, France


#### HOW TO USE IT

Download the desired Tagged version, never the current master.

To directly source a version in the environment:
source <(curl -s https://gitlab.pasteur.fr/gmillot/little_bash_functions/raw/v1.0.0/little_bash_functions.sh)

To directly save the little_bash_functions.sh file:
wget https://gitlab.pasteur.fr/gmillot/little_bash_functions/raw/v1.0.0/little_bash_functions.sh

To save all the files:
wget https://gitlab.pasteur.fr/gmillot/little_bash_functions/-/archive/v1.0.0/little_bash_functions-v1.0.0.zip

To source the local little_bash_functions.sh to have the functions available in the working environment:
source /pasteur/homes/gmillot/Git_versions_to_use/little_bash_functions-v1.0.0/little_bash_functions.sh

Description of the functions is at the beginning of the function body. To obtain it, read the little_bash_functions.sh


#### FILE DESCRIPTIONS

little_bash_functions.sh	file that has to be sourced


#### WEB LOCATION

Check for updated versions (more recent release tags) at https://gitlab.pasteur.fr/gmillot/little_bash_functions/tags


#### WHAT'S NEW IN


## v2.0

functions modified because of return. echo $? cannot be used with. Use global variables as explained inside functions


## v1.3.0

test_column_nb_perline_in_file_fun with better writting


## v1.2.0

output file name error in test_column_nb_perline_in_file_fun fixed


## v1.1.0

single_path_with_regex_fun function deals now with url


## v1.0.0

Everything
